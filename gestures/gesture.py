# Minimal class for gesture objects
#
# Copyright 2021 cunidev
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Gesture:
    def __init__(self, type, direction, command, fingers=0, enabled=True):
        self.type = type
        self.direction = direction
        self.command = command
        self.fingers = fingers
        self.enabled = enabled

    def make(self):
        fingers = str(self.fingers) + " "
        if (self.fingers == 0):
            fingers = ""

        enabled = ""
        if not self.enabled:
            enabled = "#D: "
        return (enabled + "gesture " + self.type + " " + self.direction + " " + str(fingers) + self.command)
