# Preference dialog
#
# Copyright 2021 cunidev
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk

from gestures.configfile import ConfigFileHandler


class PreferencesDialog(Gtk.Dialog):
    def __init__(self,parent, config_file):
        self.config_file = config_file
        Gtk.Dialog.__init__(self, "Preferences", parent, 0, Gtk.ButtonsType.NONE)
        self.set_transient_for(parent)
        self.set_modal(True)
        self.set_default_size(480, 100)
        self.connect("destroy", self.onDestroy)
        area = self.get_content_area()

        if(self.config_file.swipe_threshold != None):
            value = self.config_file.swipe_threshold
        else:
            value = 0

        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, margin = 10)
        area.add(box)

        label = Gtk.Label("Swipe threshold")
        slider = Gtk.Scale(orientation=Gtk.Orientation.HORIZONTAL, adjustment=Gtk.Adjustment(value, 0, 100, 5, 10, 0))
        slider.connect("value-changed", self.set_swipe_threshold)
        slider.set_hexpand(True)
        slider.set_digits(0)

        box.add(label)
        box.add(slider)

        area.show_all()

    def set_swipe_threshold(self, widget):
        value = int(widget.get_value())
        if(value >= 0 and value <= 100):
            self.config_file.swipe_threshold = value

    def onDestroy(self, window):
        self.config_file.save()

class UnsupportedLinesDialog(Gtk.Dialog):
    def __init__(self, parent, config_file):
        Gtk.Dialog.__init__(self, "Edit unsupported lines", parent, 0, Gtk.ButtonsType.NONE)
        self.set_transient_for(parent)
        self.set_modal(True)
        self.set_default_size(480, 200)
        area = self.get_content_area()


        hb = Gtk.HeaderBar()
        hb.set_show_close_button(False)

        cancelButton = Gtk.Button("Cancel")
        cancelButton.modify_bg(Gtk.StateType.ACTIVE, Gdk.color_parse('red'))
        hb.pack_start(cancelButton)

        confirmButton = Gtk.Button("Confirm")
        confirmButton.modify_bg(Gtk.StateType.ACTIVE, Gdk.color_parse('teal'))
        confirmButton.get_style_context().add_class("suggested-action")
        hb.pack_end(confirmButton)
        self.set_titlebar(hb)

        confirmButton.connect("clicked", self.on_confirm)
        cancelButton.connect("clicked", self.on_cancel)
        

        scrolledwindow = Gtk.ScrolledWindow()
        scrolledwindow.set_hexpand(True)
        scrolledwindow.set_vexpand(True)
        area.add(scrolledwindow)

        self.textview = Gtk.TextView()
        self.textbuffer = self.textview.get_buffer()

        lines = '\n'.join(config_file.rawLines)
        self.textbuffer.set_text(lines)
        scrolledwindow.add(self.textview)

        self.show_all()


    def on_cancel(self, widget):
        self.response(Gtk.ResponseType.CANCEL)

    def on_confirm(self, widget):
        self.response(Gtk.ResponseType.OK)
        
